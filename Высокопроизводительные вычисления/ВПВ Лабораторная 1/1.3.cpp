// ConsoleApplication4.cpp: определяет точку входа для консольного приложения.
//

#include <iostream>
#include <memory.h>
#include <time.h>
#include <fstream>
#include "math.h"

using namespace std;
int* sundaram(int n);

int main()
{
	clock_t start, stop;
	int N, n;
	cout << "Введите количество требуемых простых чисел:";
	cin >> n;
	if (n==1) N=3;
	else if (n==2) N=4;
	else if (n==3) N=6;
	else if (n==4) N=8;
	else if (n==5) N=12;
	else if (n==6) N=14;
	else if (n==7) N=18;
	else if (n==8) N=20;
	else if (n==9) N=24;
	else if (n==10) N=30;
	else if (n<=100) N=542;
	else if (n<=1000) N=7920;
	else if (n<=10000) N=104730;
	else N=n*log((double)n)+n*log(log((double)n))-n*0.9;
	bool *ar=new bool[N]; 
	start = clock ();

	//Заполняем массив нулями
	memset(ar,false,N);

	//Алгоритм
	int *b=sundaram(N);
	
	stop = clock();
	


	//Вывод времени
	int msek_time = stop - start;
	cout << "\nЗатраченое время (мсек): " << msek_time << endl;
	
	//Запись в файл
	ofstream out;
	out.open("output.txt");
	out << msek_time << endl;
	out << 2 << endl;
	int m=2;
	cout << "Идёт запись в файл..." << endl;
	for (int i=1;i<N;++i)
	if ((b[i]==0) && (m<=n)){ 
	out << (2 * i + 1) << endl;
	m++;
	}
	out.close();
	cout << "Запись в файл закончена" << endl;
	
	return 0;
}

int* sundaram(int n){
    int *a = new int [n], i, j, k;
    memset(a, 0, sizeof(int) * n);
    for(i = 1; 3*i+1 < n; i++){
 	  for(j = 1; (k = i+j+2*i*j) < n && j <= i; j++){
 		a[k] = 1;
 	  }
    }
    
	  return a;

}