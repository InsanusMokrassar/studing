#include <iostream>
#include <fstream>
#include <ctime>
#include <memory.h>
#include <math.h>

using namespace std;

#pragma warning (disable: 4996)

#define number_entered 15485863
ofstream file;
int size = -1;
double t;

#define max_val 15485863 // million
//#define max_val 50
#define kolvo 100  // менять для количества чисел
#define min_val 2

int primes[kolvo];

// Алгоритм: начинаем с числа 2. Выводим текущее число, "вычеркиваем" числа, кратные текущему, ищем следующее невычеркнутое число.
int main ()
{
	cout << "This program lists 1000000 prime numbers.\n";
	
	  t = clock();
 // Поиск простых ведем до корня из max_val
    int sqrt_max_val = (int)sqrt((double)max_val);
// Признаки "вычеркнутости" чисел (элементы 0 и 1 не используются)
    char *flags = 0;
    try
    {
      flags = new char [max_val+1];  
// первоначально все числа не перечеркнуты
      memset (flags, 0, max_val+1);
      int current_value = 2;
	  if (size <= kolvo) primes[size++] =  current_value;
      do
      {
         if (current_value >= min_val)
            if (size < kolvo) primes[size++] =  current_value;
// Вычеркиваем все числа, кратные current_value, начиная с его квадрата
    for (int i = current_value * current_value; i <= max_val; i += current_value)
    flags[i] = true;
// Ищем следующее невычеркнутое число
    for (++current_value; current_value <= sqrt_max_val && flags[current_value]; ++current_value) ;
      } while (current_value <= sqrt_max_val);
// Выведем все невычеркнутые числа больше корня из N, при этом не рассматриваем числа менее min_val
    for (current_value = min_val > sqrt_max_val+1 ? min_val : sqrt_max_val+1; current_value <= max_val; ++current_value)
    if (!flags[current_value])
            if (size < kolvo) primes[size++] =  current_value;
    }
    catch(...)
    {
        if (flags != 0)
            delete[] flags;
    };

	t = clock() - t;
	cout << "\nGenerating lasts " << t << " milliseconds;\n";
//_________________________________________________________________________

file.open("primes.txt");
	file << "Generating lasts " << t << " milliseconds;\n\n";
for(int i = 0; i < size; i++){
	file << primes[i];
	if (i != size - 1) file << "\n";
	else file << " ";
}
file.close();
cout << "Finished";

return 0;
}