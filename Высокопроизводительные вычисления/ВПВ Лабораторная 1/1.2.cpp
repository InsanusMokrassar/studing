#include "math.h"
#include "time.h"
#include <fstream> 
#include <iostream>
#include <locale.h>
using namespace std;

int main()
{
	setlocale(LC_ALL, "RUS");

	unsigned int N;
	int count1=1;
	cout << endl << "	Ââåäèòå êîëè÷åñòâî ýëåìåíòîâ: ";
	N = 1000000;
	double size1 = (log((double)N)+2)*N;
	int size = (int)size1;
	bool *num = new bool[size];
	int *m1 = new int[N];
	m1[0]=2;

	clock_t time;
	time = clock();

	for (int i=1;i<size;i+=2) num[i]=true; 
	for (int i=2;i<size;i+=2) num[i]=false;
	num[1]=false;

	int sqrt_size = (int)sqrt(size+1.0);
	for (int i=3;i<sqrt_size;i+=2)
	{
		if (num[i])
		{
			int count=i;
			while (i*count<size)
			{
				num[i*count++]=false;
				
			}
		}
	}
	for (int i=2;i<size;i++)
	{
		if (num[i] && count1 < N) 
		{
			m1[count1] = i;
			count1++;
		}
	}

	time = clock() - time;
	cout << endl << "	Âðåìÿ âû÷èñåëíèÿ: " << time << " ìñ";

	ofstream out;
	out.open("output.txt");
	out << time << endl;
	for(int k=0; k<N; k++)
	{
		out << m1[k] << endl;
	}
	cout << endl << "	" << N << " ïðîñòûõ ÷èñåë çàïèñàíû â ôàéë output.txt";

	return 0;
}