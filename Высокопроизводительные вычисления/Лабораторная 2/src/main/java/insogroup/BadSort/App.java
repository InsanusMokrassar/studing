package insogroup.BadSort;

import insogroup.handlers.exceptions.HandlerException;
import insogroup.handlers.fields.IOFields;
import insogroup.handlers.fields.SortingFields;
import org.json.JSONObject;

/**
 * Hello world!
 *
 */
public class App 
{

    protected String resourcesPrefix = "";

    public static void main( String[] args ) {
        try {
            new App().run(args);
        } catch (HandlerException e) {
            e.printStackTrace();
        }
    }

    public void run(String[] args) throws HandlerException {
//        if (args.length > 0) {
//            resourcesPrefix = args[0];
//            System.out.println("root changed to : " + resourcesPrefix);
//        }
//        try {
//            HandlersSystemExecutor executor = SystemInitializer.init(
//                    InputStreamReader.getInstance(new FileInputStream(resourcesPrefix + "config.json")),
//                    OutputStreamWriter.getInstance(System.out)
//            );
////                    SystemInitializer.init(
////                    ,
////                    InputStreamReader.getInstance(new FileInputStream(resourcesPrefix + "config.json")),
////                    );
////            StringSorter sorter = new StringSorter(StringScanner.getInstance(testString).generateTable(), param);
//            JSONObjectReader r = JSONObjectReader.getInstance(InputStreamReader.getInstance(new FileInputStream(resourcesPrefix + "commands.json")));
//            JSONObject command = new JSONObject(r.read());
//            executor.receive(command);
//            r.reset();
//            command = new JSONObject(r.read());
//            executor.receive(command);
//        } catch (FileNotFoundException e){
//            System.out.println("can't find file: " + e.getMessage());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        JSONObject message = new JSONObject(
                "{" +
                        "\"target\":\"" + Fields.GENERATED_WEIGHTS + "\"," +
                        "\"generatorInfo\":[" +
                            "\"!-/\"," +
                            "\":-@\"," +
                            "\"[-`\"," +
                            "\"0-9\"," +
                            "\"а-я\"," +
                            "\"А-Я\"," +
                            "\"a-z\"," +
                            "\"A-Z\"" +
                        "]" +
                "}");
        new SymbolsWeightsGeneratorHandler().handle(message);

        message.put(SortingFields.INPUT_FILE_FIELD, args[0]);
        message.put(IOFields.TARGET_FIELD, args[1]);
        new SorterHandler().handle(message);
    }
}
