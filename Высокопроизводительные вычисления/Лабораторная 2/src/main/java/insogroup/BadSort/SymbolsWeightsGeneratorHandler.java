package insogroup.BadSort;

import insogroup.handlers.exceptions.HandlerException;
import insogroup.handlers.fields.IOFields;
import insogroup.handlers.interfaces.Handler;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SymbolsWeightsGeneratorHandler implements Handler {

    /**
     * <pre>
     * Awaits: {
     *     "generatorInfo" : [<----------------------------------|
     *         "a-b",                                            |
     *         "1-2",                                            | Info for generator
     *         "ф-я",                                            |
     *         "3-3"                                             |
     *     ],<---------------------------------------------------|
     *     "target": "NAME_OF_TARGET_FIELD"
     * }
     * </pre>
     * @param jsonObject
     * @throws HandlerException
     */
    @Override
    public void handle(JSONObject jsonObject) throws HandlerException {
        String targetField = jsonObject.getString(IOFields.TARGET_FIELD);

        JSONArray rules = jsonObject.getJSONArray(Fields.GENERATOR_INFO);

        Map<String, Integer> targetRules = new HashMap<>();

        Integer currentWeight = 0;

        for (int i = 0; i < rules.length(); i++) {
            String currentRule = rules.getString(i);
            String[] symbols = currentRule.split("-");
            for (int j = symbols[0].codePointAt(0); j < symbols[1].codePointAt(0); j++) {
                targetRules.put(((char) j) + "", currentWeight);
                currentWeight++;
            }
        }

        jsonObject.put(targetField, targetRules);
    }
}
