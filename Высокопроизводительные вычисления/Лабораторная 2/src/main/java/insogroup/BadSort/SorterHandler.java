package insogroup.BadSort;

import insogroup.handlers.exceptions.HandlerException;
import insogroup.handlers.fields.IOFields;
import insogroup.handlers.fields.SortingFields;
import insogroup.handlers.interfaces.Handler;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.*;

/**
 * Created by aleksey on 12.04.16.
 */
public class SorterHandler implements Handler {

    /**
     * Awaits: {
     *     "generatedCodesWeights" : {
     *         "a": 0,
     *         "c": 23,
     *         ...
     *     },
     *     "inFile" : "PATH_TO_SRC_FILE",
     *     "target": "PATH_TO_DEST_FILE"
     * }
     * @param jsonObject
     * @throws HandlerException
     */
    @Override
    public void handle(JSONObject jsonObject) throws HandlerException {
        Date start = new Date();
        String srcField = jsonObject.getString(SortingFields.INPUT_FILE_FIELD);

        JSONObject weightsObject = jsonObject.getJSONObject(Fields.GENERATED_WEIGHTS);

        new HashMap<String, Integer>();

        List<String> outList = new LinkedList<>();

        try {
            FileReader srcReader = new FileReader(srcField);
            BufferedReader reader = new BufferedReader(srcReader);
            String currentLine = reader.readLine();
            do {
                Integer resultIndex = outList.size();
                for (int i = 0; i < outList.size(); i++) {
                    String currentNewSymb;
                    String currentOldSymb;
                    Boolean equal = false;
                    for (int j = 0; j < currentLine.length() && j < outList.get(i).length(); j++) {
                        currentNewSymb = currentLine.substring(j, j+1);
                        currentOldSymb = outList.get(i).substring(j, j+1);
                        try {
                            Integer currentWeight = weightsObject.getInt(currentNewSymb);
                            Integer oldWeight = weightsObject.getInt(currentOldSymb);
                            if (currentWeight.equals(oldWeight)) {
                                equal = true;
                                continue;
                            }
                            if (currentWeight > oldWeight) {
                                equal = false;
                                break;
                            }
                            if (currentWeight < oldWeight) {
                                equal = false;
                                resultIndex = i;
                                break;
                            }
                        } catch (JSONException e) {
                            continue;
                        }
                    }
                    if (equal && (currentLine.length() < outList.get(i).length())) {
                        resultIndex = i;
                    }
                    if (resultIndex != outList.size()) {
                        break;
                    }
                }
                outList.add(resultIndex, currentLine);
                currentLine = reader.readLine();
            } while (currentLine != null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Date finish = new Date();
            FileWriter outFile = new FileWriter(jsonObject.getString(IOFields.TARGET_FIELD));
            outFile.write("Time (in millis): " + (finish.getTime() - start.getTime()) + "\n");
            for (String current : outList) {
                outFile.write(current + "\n");
            }
            outFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
