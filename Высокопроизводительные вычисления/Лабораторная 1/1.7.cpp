#include <cstdlib>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <fstream>

using namespace std;

int* primes;
int xcount;

bool isPrime(int number) {
    int s = (int) sqrt((double) number) + 1;

    for (int i = 0; i < xcount && primes[i] < s; i++) {
        if (number % primes[i] == 0) {
            return false;
        }
    }
    return true;
}

void eratosphen(int N) {
    int size = 0.75 * log((double) N) / log( 2.0 ) * N;
    bool *numbers = new bool[size];
    for (int i = 0; i < size; i++) {
        numbers[i] = true;
    }

    for (int i = 2; i < size; i++) {
        if (!numbers[i]) continue;
        for (int j = i + i; j < size; j += i) {
            numbers[j] = false;
        }
    }
    xcount = 0;
    for (int i = 2; i < size; i++) {
        if (numbers[i]) {
            primes[xcount] = i;
            xcount++;
        }
    }
    delete[] numbers;
}

int main(int argc, char** argv) {

    int N = 1000000000;
    printf("%d\n", N);
    clock_t time = clock();

    primes = new int[N];
    eratosphen(N);
    int current = primes[xcount - 1] + 2;
    while (xcount < N) {
        if (isPrime(current)) {
            primes[xcount] = current;
            xcount++;
        }
        current += 2;
    }
    time = clock() - time;
    printf("%f\n", time * 1.0 / CLOCKS_PER_SEC);
	ofstream file("primes.txt");
	file<<time<<endl;
	for (int i=0;i<N;i++)
	{
		file<<primes[i]<<endl;
	}	
	file.close();
    delete[] primes;
	system("pause"); 
	return 0;
}

