#include <iostream>
#include <iomanip>
#include <fstream>
#include <ctime>
using namespace std;
int main()
{
	setlocale(LC_ALL, "Russian");
	ofstream MyFile("Primes.txt");
	clock_t start = clock();
	int i, j;
	double duration;
	const int n = 15485864;
	bool *a = new bool[n];
	for (i = 0; i < n; i++)
	{
		a[i] = true;
	};
	for (i = 2; i < n; i++)
	{
		if (a[i] == true)
		{
			for (j = i; j < n; j += i)
			{
				a[j] = false;
			}
			a[i] = true;
		}
	}
	duration = (clock() - start) / (double)CLOCKS_PER_SEC;
	cout << "����� ��� ������: " << duration << endl;
	for (int i = 2; i < n; i++)
	{
		if (a[i] == true)
		{
			MyFile << i << "\n";

		}
	}
	delete[] a;
	duration = (clock() - start) / (double)CLOCKS_PER_SEC;
	cout << "����� c �������: " << duration << endl;
	system("Pause");
	return 0;
}