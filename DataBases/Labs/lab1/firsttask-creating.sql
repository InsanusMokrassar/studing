CREATE TABLE drug (
            id INTEGER PRIMARY KEY,
            drug_count INTEGER,
            drug_cost INTEGER,
            name VARCHAR(45) UNIQUE
        );
CREATE TABLE benefit(
            id INTEGER PRIMARY KEY, 
            value INTEGER, 
            name VARCHAR(45) UNIQUE
        );
CREATE TABLE client(
            id INTEGER PRIMARY KEY, 
            name VARCHAR(45)
        );
CREATE TABLE benefit_client_link(
            benefit_id INTEGER, 
            client_id INTEGER, 
            FOREIGN KEY (benefit_id) REFERENCES benefit(id), 
            FOREIGN KEY (client_id) REFERENCES client(id)
        );

CREATE TABLE drug_order(
            id INTEGER PRIMARY KEY,
            order_date DATE,
            client_id INTEGER,
            FOREIGN KEY (client_id) REFERENCES client(id)
        );
CREATE TABLE order_part(
            id INTEGER PRIMARY KEY,
            sum_of_drugs INTEGER,
            order_id INTEGER,
            drug_id INTEGER,
            FOREIGN KEY (order_id) REFERENCES drug_order(id),
            FOREIGN KEY (drug_id) REFERENCES drug(id)            
        );