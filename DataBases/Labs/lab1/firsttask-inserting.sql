INSERT INTO drug (id, drug_count, drug_cost, name) values (0, 12, 20, '�������');
INSERT INTO drug (id, drug_count, drug_cost, name) values (1, 7, 28, '���������');
INSERT INTO drug (id, drug_count, drug_cost, name) values (2, 53, 3, '��������');

INSERT INTO benefit (id, value, name) values (0, 2, '���������');
INSERT INTO benefit (id, value, name) values (1, 5, '���������� ����������');

INSERT INTO client (id, name) values(0, '������� ��������');
INSERT INTO client (id, name) values(1, '���� ��������');
INSERT INTO client (id, name) values(2, '������� ���������');

INSERT INTO benefit_client_link(benefit_id, client_id) values(0, 0);
INSERT INTO benefit_client_link(benefit_id, client_id) values(0, 1);
INSERT INTO benefit_client_link(benefit_id, client_id) values(1, 0);
INSERT INTO benefit_client_link(benefit_id, client_id) values(1, 1);

INSERT INTO drug_order(id, order_date, client_id) values(0, null, 0);
INSERT INTO order_part(id, sum_of_drugs, order_id, drug_id) values (0, 4, 0, 0);
INSERT INTO order_part(id, sum_of_drugs, order_id, drug_id) values (1, 7, 0, 1);

INSERT INTO drug_order(id, order_date, client_id) values(1, null, 1);
INSERT INTO order_part(id, sum_of_drugs, order_id, drug_id) values (2, 2, 1, 0);
INSERT INTO order_part(id, sum_of_drugs, order_id, drug_id) values (3, 1, 1, 2);

SELECT * FROM client;
SELECT client.name, drug.name FROM client, drug, drug_order, order_part 
      WHERE 
        drug_order.client_id=client.id and
        order_part.order_id=drug_order.id and
        drug.id=order_part.drug_id;