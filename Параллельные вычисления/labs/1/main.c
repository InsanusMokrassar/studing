#include <mpi.h>
#include <stdio.h>
#include <time.h>

int main(int argc, char *argv[])
{
	int rank, size, namelen;
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	time_t t;
	t = time(NULL);
	printf("%d\n", localtime(&t)->tm_sec);
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Get_processor_name(processor_name, &namelen);
	printf("Factorial of %d = %d, process %d on %s from %d\n", size, factorial(size), rank, processor_name, size);
	MPI_Finalize();
	t = time(NULL);
	printf("%d\n", localtime(&t)->tm_sec);
	return 0;
}

int factorial(int a){
	if (a>1){
		return a*factorial(a-1);
	}
	return a;
}