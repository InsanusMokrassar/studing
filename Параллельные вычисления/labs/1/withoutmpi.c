#include <stdio.h>
#include <time.h>

int main(int argc, char *argv[])
{
	int size = 8;
	time_t t;
	t = time(NULL);
	printf("%d\n", localtime(&t)->tm_sec);
	int i;
	for (i = 0; i < size; i++){
		printf("Factorial of %d = %d, step %d from %d\n", size, factorial(size), i, size);
	}
	t = time(NULL);
	printf("%d\n", localtime(&t)->tm_sec);
	return 0;
}

int factorial(int a){
	if (a>1){
		return a*factorial(a-1);
	}
	return a;
}