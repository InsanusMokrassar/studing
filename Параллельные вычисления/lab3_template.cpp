#include "mpi.h"
#include <stdio.h>
#include <math.h>

int calculatePartialSum(int start, int quantity, int step, double &partialSum){
	partialSum=0.0;
	for(int i=start+1; i<quantity; i+=step){ //start+1, �.�. ��� ���������� � 1
		partialSum += 4.0/( 1 + pow(( ( (double)i-0.5 ) / quantity), 2) );
	}
	partialSum /= quantity;
	return 0;
}

void helloFromProcess(int number, char name[]){
/*
	�������, ��������� ����� �������� (number) � ��� ���������� (name), �� ������� �� �������
*/
	fprintf(stdout, "Process number %d on %s\n", number, name);
	fflush(stdout);
}

int initAndGetInfo(int argc, char *argv[], int &numberOfProcesses, int &localId, char localName[]){
/*
	�������, ���������������� MPI � ���������� ������� ���������� � ��������� (���������� ���������, ����� �������� �������� � ��� ����������).
	� ������ ������ ��� ������ ������� ������� ���������� ����������� ��� ������.
*/
	int localNameLength;
	if (MPI_Init(&argc, &argv) != MPI_SUCCESS)
		return 1;
	if (MPI_Comm_size(MPI_COMM_WORLD, &numberOfProcesses) != MPI_SUCCESS)
		return 2;
	if (MPI_Comm_rank(MPI_COMM_WORLD, &localId) != MPI_SUCCESS)
		return 3;
	if (MPI_Get_processor_name(localName, &localNameLength) != MPI_SUCCESS)
		return 4;
	return 0; //if everything is ok
}

void askForANumber(int &number){
/*
	�������, ������������� � ������������ �����
*/
	printf("Enter the number of intervals: (0: exit) ");
	fflush(stdout);
	scanf("%d", &number);
}

void getStatistics(int start, int quantity, int step, double &combinedSum, double *localTimeArray){
	/* ������� ��� ���������� ������ quantity ��������� ����. 
	��������� (����� �����) �������� � combinedSum, ����� ���������� �������� ������ calculatingTime ���������� � ������ localTimeArray
	*/
	
	double partialSum = 0.0;
	double calculatingTime = MPI_Wtime();
	calculatePartialSum(start, quantity, step, partialSum);
	calculatingTime = MPI_Wtime() - calculatingTime;
	MPI_Reduce(&partialSum, &combinedSum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Gather(&calculatingTime, 1, MPI_DOUBLE, localTimeArray, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD); //localTimeArray ���� �� ������
}

void calculatePortions(double *timeArray, int numberOfProcesses, int *startingPoint, int sizeOfPortion, int *arrayOfSizes){
	/*timeArray - ������ � �������� ���������� �������� ������
	numberOfProcesses - ���������� ���������
	startingPoint - ������ ��������� ����� (�������� ������������ 0) ��� ������� ��������
	sizeOfPortion - ������ ������, ������� ����� �������� �� ����� � ����������� �� �������
	arrayOfSizes - ������ � ����������� ������ ��� ������� ��������
	
	���������� ������� ������, � ������� ����� ��������� ��������:
	- ������� ���������������� ���, ������� ����� � timeArray
	- � ����� ���������������� ����� ���� ��������� timeArray
	�.�., ���� timeArray = {2,5,3}, �� ����� ��������� ����� 10, � ����� ������ ����� ����� {5,2,3.333333}
	
	
	*/

	//������� ����������, � ������� ����� ��������� ����� ����� ����������
	//� ����� �������� �� ������� timeArray � �������� ����� ����� �������
	//������� ������, � ������� ����� ��������� "��������" �������
	//� ����������, ��� ����� ����� ���� �������� �����
	//� ����� ��������� ���� ������ (������ i-� ������� ����� ����� �����, ��������� �� i-� ������� �� ������� timeArray)
	//� ��� �� ����� ��������� ����� �������� �����

	//������� ����������-�����������, ������� ����� ����� ��������� sizeOfPortion � ����� �������� �����
	//������� ����������, � ������� ����� ��������� ����� ���� ������ (���������� ��� ����, ����� �������� ������� ��� ����������)
	//�������� �������� ������� startingPoint ��������� �������� 0, �.�. ������ ����� ���������� � �������� ��������

	//� ����� ��������� ������ arrayOfSizes (������ i-� ������� ����� ������������ i-�� �������� ������� � ��������� ��������� �� �����������; ��� ������������ ����� �������� � ������ ����)
	//� ���������� � ������ ���� ������ ��������� ������� ������� ������� arrayOfSizes (������� ������ ��� ��� ��������)
	//���� i-� ������� �� �������� ��������� � �������, �� ��������� �������� startingPoint[i+1] ����� ���� ������

	//������� ����������, ������� ��������� �������� ����� sizeOfPortion � ������ ���� ������
	//���� ��� �� ����� ����, �� � ���������� �������� ������� arrayOfSizes ��������� ��� �������
}


void calculateWeightenedSum(int startingPoint, int localPortion, int sizeOfPortion, int totalQuantity, double &partialSum){  
	/*
	startingPoint - ������ ������ (����� ������������ 0)
	localPortion - ����� �����
	sizeOfPortion - ������ ������, ������� ����� �������� �� ����� � ����������� �� �������
	totalQuantity - ����� ���������� ���������
	partialSum - ��������� ��������� �����
	*/

	//������� ���������� tempSum (���� double), � ������� ����� ��������� ��������� �����
	//��������� ���������� partialSum, ��� �������� ��������� �����, �������� 0.0
	//������� ���� �� ���������� ���������: ������� (��������, i)= startingPoint, ������� �� ������ ���� ������ totalQuantity, ������� ������������� �� sizeOfPortion
		//���� ����� �������� � localPortion �� ��������� totalQuantity, �� ��������� � ���� ����� ��������� ����� �� i �� i+localPortion, ��� ����� 1, ��������� ������������ � tempSum
		//���� ���������, �� ������� �� i �� totalQuantity. ��� ������� ��� ��� �������, ����� ����� ����� ��������� �� ������ ������, ����� �� ������� ������.
		//����� ����� � partialSum ������������ �������� tempSum
}



void main(int argc, char *argv[]){
	int numberOfElements, localId, numberOfProcesses;
	const double PI25DT = 3.141592653589793238462643;
	double localPartialSum, combinedSum;
	double startingTime;
	int ROOT_ID = 0;
	char localName[MPI_MAX_PROCESSOR_NAME];

	initAndGetInfo(argc, argv, numberOfProcesses, localId, localName);
	helloFromProcess(localId, localName);
	while(true){
		if(localId == ROOT_ID){
			askForANumber(numberOfElements);
			startingTime=MPI_Wtime();
		}
		MPI_Bcast(&numberOfElements, 1, MPI_INT, ROOT_ID, MPI_COMM_WORLD);
		if(numberOfElements == 0)
			break;
		else{
			/* ��� ������ ���� �� ��� ������
			calculatePartialSum(localId, numberOfElements, numberOfProcesses, localPartialSum);
			MPI_Reduce(&localPartialSum, &combinedSum, 1, MPI_DOUBLE, MPI_SUM, ROOT_ID, MPI_COMM_WORLD);
			*/
			double * timeArray = new double[numberOfProcesses];
			getStatistics(0, numberOfElements,numberOfProcesses,combinedSum,timeArray);
			// ��������� ������
			int * startingPoints = new int[numberOfProcesses];
			int * portions = new int[numberOfProcesses];
			if(localId == ROOT_ID){
				//���������� �������� ������ � ������ ������������ ������� ��������
				calculatePortions(timeArray,numberOfProcesses,startingPoints,10000,portions);
			}
			MPI_Bcast(startingPoints, numberOfProcesses, MPI_INT, ROOT_ID, MPI_COMM_WORLD);
			MPI_Bcast(portions, numberOfProcesses, MPI_INT, ROOT_ID, MPI_COMM_WORLD);
			//���������� ����� � ������ ���������� ����������
			calculateWeightenedSum(startingPoints[localId], portions[localId], 10000, numberOfElements, localPartialSum);
			printf("sum on process %i = %f\n",localId, localPartialSum);
			fflush(stdout);
			MPI_Reduce(&localPartialSum, &combinedSum, 1, MPI_DOUBLE, MPI_SUM, ROOT_ID, MPI_COMM_WORLD);
			if(localId == ROOT_ID){
				printf("Pi is approximately %.16f, Error of Pi is %.16f\n", combinedSum, fabs(combinedSum - PI25DT));
				printf("Calculation time = %f\n", MPI_Wtime() - startingTime);
				fflush(stdout);

				// ��� ������
				// ����� �� ��������
				for(int i=0; i<numberOfProcesses;++i){
					printf("time on process %i = %f start = %i size = %i\n",i, timeArray[i], startingPoints[i], portions[i]);
					fflush(stdout);
				}

			}
		}
	}
	MPI_Finalize();
	return;
}
	

	