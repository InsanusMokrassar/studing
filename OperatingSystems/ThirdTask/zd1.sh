#!bin/sh

finish(){
	echo Нажмите Enter для продолжения...
	read x
	exit 0
}
echo Введите нужный вам пункт:
echo 1. Создает отчет о количестве доступных файловых систем, их свободном и занятом пространстве
echo 2. Выводит информацию об имени и размере файла отчета, а также о правах пользователя на него
read checking
if test $checking -eq 1 
	then
	df > info.txt
	echo  Создан отчет о количестве доступных файловых систем, их свободном и занятом пространстве
fi
if test $checking -eq 2
	then
	ls -all info.txt
fi
finish