#!bin/sh

finish(){
	echo Нажмите Enter для продолжения...
	read x
	exit 0
}

saveFilesInfo(){
	ls -l -i -c -r $path > $reportName
}

openAccessForReportFile(){
	chmod 777 $reportName
}

createLinks(){
	rm ~/$reportName
	ln $reportName ~/$reportName
	rm ~/Desktop/$reportName
	rm ~/Рабочий\ стол/$reportName
	ln -s $startDirectory/$path/$reportName ~/Desktop/$reportName
	ln -s $startDirectory/$path/$reportName ~/Рабочий\ стол/$reportName #For russian versions
}

startDirectory=$(pwd)
echo Введите название директории:
read path
reportName="report.txt"
rm $reportName
if test -d $path
	then
	saveFilesInfo
	openAccessForReportFile
	createLinks
fi
finish

