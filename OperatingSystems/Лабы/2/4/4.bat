Setlocal EnableDelayedExpansion
@echo off
:readsourcetop
cls
set /p sourcetop="Enter path of input directory or /help for help: "
if [%sourcetop%]==[/help] (
cls
echo Help
echo ������ ��� ��室���� ��⠫���, ��⥬ ��� १�ࢭ��� ��⠫���. � ��砥, �᫨ ��� १�ࢭ��� ��⠫��� �� �㤥� 㪠����, � �㤥� �ᯮ�짮��� ��⠫�� backup. ��⥬ � ���� �롥�� ���� �� ���� ०���� ����஢���� - ����饭�� ����� 䠩��� � ���������� �����, ⮫쪮 ���������� ����� 䠩��� � 㤠����� ����� 䠩���.
pause
goto readsourcetop
)
cd %sourcetop%||(pause & goto readsourcetop)
set sourcetop=%cd%
cd ..
set thispath=%cd%
set /p sourceback="Enter path of output directory: "
if [%sourceback%]==[] (
echo Using current directory
set sourceback=%thispath%\backup
)
if not exist %sourceback% md %sourceback%
cd %sourceback%
set sourceback=%cd%
cd %thispath%
:menu
cls
echo Menu:
echo 1. Replace files
echo 2. Add new files
echo 3. Delete old files
set /p copyvar=""
if [%copyvar%] == [1] (
xcopy %sourcetop% %sourceback% /e /y
) else (
if [%copyvar%] == [2] (
for /r %sourcetop% %%i in (*.*) do (
set tempname=%%~fi
set tempname=!tempname:%cd%=!
set tempname=!tempname:~1!
set tempname=!tempname:%%~ni%%~xi=!
set cuttempname=!tempname:*\=!
echo "!thispath!\!tempname!%%~ni%%~xi"
echo "!sourceback!\!cuttempname!%%~ni%%~xi"
if not exist "!sourceback!\!cuttempname!" md "!sourceback!\!cuttempname!"
if not exist "!sourceback!\!cuttempname!%%~ni%%~xi" copy "!thispath!\!tempname!%%~ni%%~xi" "!sourceback!\!cuttempname!%%~ni%%~xi"
)
) else (
if [%copyvar%] == [3] (
for /r %sourceback% %%i in (*.*) do (
set tempname=%%~fi
set tempname=!tempname:%cd%=!
set tempname=!tempname:~1!
set tempname=!tempname:*\=!
if not exist "!%sourcetop%\!tempname!" del /q "%%~fi"
)
) else (
goto menu
)
)
)
rem cls
echo success
pause