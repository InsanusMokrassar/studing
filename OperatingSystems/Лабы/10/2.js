//			Main

FSO = WScript.CreateObject("Scripting.FileSystemObject")
var OutputName = "log.txt"
Log = FSO.OpenTextFile(OutputName, 2, true)
Drives = FSO.Drives.E
var Root = FSO.GetFolder("D:\\")
Tree(FSO, Root)

function Tree(FSO, Folder)
{
	var Files = new Enumerator(Folder.Files)
	for(0;!Files.atEnd();Files.moveNext())
	{
		var File = Files.item()
		if ((FSO.GetExtensionName(File)) == "tmp")
		{
			Log.Write(FSO.GetAbsolutePathName(File) + " ")
			try 
			{
				File.Delete(0)
				Log.WriteLine("OK")
			}
			catch(Exception)
			{
				Log.WriteLine("Busy")
			}
		}
	}
	var SubFolders = new Enumerator(Folder.SubFolders)
	for(0;!SubFolders.atEnd();SubFolders.moveNext()) Tree(FSO, SubFolders.item())
}