//			Main

FSO = WScript.CreateObject("Scripting.FileSystemObject")
var OutputName = ""
WScript.StdOut.Write("Type list file name: ")
OutputName = WScript.StdIn.Readline()
File = FSO.OpenTextFile(OutputName, 2, true)
Drives = new Enumerator(FSO.Drives)
for(0;!Drives.atEnd();Drives.moveNext()) DiskInfo(Drives.item(), File)



function DiskInfo(Drive, File)
{
	if (Drive.IsReady)
	{
		File.Writeline("+----------------------------------------------------------+")
		File.Writeline("|                         Disk " + Drive.DriveLetter + ":                          |")
		File.Writeline("+----------------------------------------------------------+")
		var Tmp = "|VolumeName: " + Drive.VolumeName
		File.Write(Fill(Tmp) + "|")
		Tmp = "Fullsize, MB: " + Math.round(Drive.TotalSize/1048576)
		File.Writeline(Fill(Tmp) + "|")
		File.Writeline("+----------------------------------------------------------+")
		Tmp = "|UsedSpace, MB: " + Math.round((Drive.TotalSize - Drive.FreeSpace)/1048576)
		File.Write(Fill(Tmp) + "|")
		Tmp = "FreeSpace, MB: " + Math.round(Drive.FreeSpace/1048576)
		File.Writeline(Fill(Tmp) + "|")
		File.Writeline("+----------------------------------------------------------+")
	}
	else File.Writeline("Disk " + Drive.DriveLetter + ": is not ready")
	File.Writeline(" ")
}

function Fill(str)
{
	var n = 28
	for (var i = str.length; i<=n; i++) str += " "
	return str
}