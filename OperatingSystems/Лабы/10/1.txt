+----------------------------------------------------------+
|                         Disk C:                          |
+----------------------------------------------------------+
|VolumeName: System          |Fullsize, MB: 32154          |
+----------------------------------------------------------+
|UsedSpace, MB: 29464        |FreeSpace, MB: 2690          |
+----------------------------------------------------------+
 
+----------------------------------------------------------+
|                         Disk D:                          |
+----------------------------------------------------------+
|VolumeName: Program         |Fullsize, MB: 307200         |
+----------------------------------------------------------+
|UsedSpace, MB: 38124        |FreeSpace, MB: 269076        |
+----------------------------------------------------------+
 
+----------------------------------------------------------+
|                         Disk E:                          |
+----------------------------------------------------------+
|VolumeName: Media, Other    |Fullsize, MB: 614400         |
+----------------------------------------------------------+
|UsedSpace, MB: 285493       |FreeSpace, MB: 328907        |
+----------------------------------------------------------+
 
Disk F: is not ready
 
+----------------------------------------------------------+
|                         Disk G:                          |
+----------------------------------------------------------+
|VolumeName: OLBOR           |Fullsize, MB: 1968           |
+----------------------------------------------------------+
|UsedSpace, MB: 171          |FreeSpace, MB: 1797          |
+----------------------------------------------------------+
 
+----------------------------------------------------------+
|                         Disk W:                          |
+----------------------------------------------------------+
|VolumeName: POLIGON         |Fullsize, MB: 13             |
+----------------------------------------------------------+
|UsedSpace, MB: 5            |FreeSpace, MB: 8             |
+----------------------------------------------------------+
 
