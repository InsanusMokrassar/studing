WScript.StdOut.WriteLine("������� N - ������ ������� (NxN)")
var n = WScript.StdIn.ReadLine()
var matrix = [];
var countOfSeven = 0
var countOfOdd = 0
CreateMatrix(matrix, n)
WScript.StdOut.WriteLine("�������� �������: ")
ShowMatrix(matrix, n)
CountOfSeven(matrix)
OddLineElemSumm(matrix)
OddColumnEvenElemMul(matrix)
SummHigherSideDiag(matrix)
var sortmatrix = []
for (var i = 0; i < n; i++) {
    sortmatrix[i] = []
    for (var j = 0; j < n; j++) sortmatrix[i][j] = matrix[i][j]
}
SortMatrix(sortmatrix)
WScript.StdOut.WriteLine("���������� ������ ������ ������ ������� �� �����������, ������ �������� � �� ��������")
ShowMatrix(sortmatrix, n)
var obor = ObrMatrix(matrix, n)
WScript.StdOut.WriteLine("�������� �������: ")
ShowMatrix(obor, n)
mulmatr = MulMatrix(matrix, sortmatrix)
WScript.Echo("��������� �������� ������� �� �������������: ")
ShowMatrix(mulmatr, n)
WScript.Echo("����� ���������� ��������� � ���� ������: ")
WScript.StdOut.WriteLine(mulmatr)
b = WScript.StdIn.ReadLine()

function CreateMatrix(mass, len) {
    for (var i = 0; i < len; i++) {
        matrix[i] = []
        for (var j = 0; j < len; j++) {
            if (i == j) {
              mass[i][j] = 1
              continue
            }
            if (i < j){ 
              mass[i][j] = 7
              continue
            }
            mass[i][j] = 4
        }
    }
}

function ShowMatrix(mass, n, SqrOrStr) {
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < n; j++) {
            WScript.StdOut.Write(mass[i][j])
            WScript.StdOut.Write(" ")
        }
        WScript.StdOut.WriteLine()
    }
}

function CountOfSeven(mass) {
    var countOfSeven = (n*n - n) / 2
    WScript.StdOut.WriteLine("���������� ��������� ������� 7:  " + countOfSeven)
}

function OddLineElemSumm(mass) {
    var summ = 0
    for (var i = 0; i < n; i += 2) {
        for (var j = 0; j < n; j++) {
            summ += mass[i][j]
        }
    }
    WScript.StdOut.Write("����� ��������� ������� � ������ �������� ������:  ")
    WScript.StdOut.WriteLine(summ)
}

function OddColumnEvenElemMul(mass) {
    var res = 1
    for (var j = 0; j < n; j += 2) {
        for (var i = 1; i < n; i += 2) {
            res *= mass[i][j]
        }
    }
    WScript.StdOut.WriteLine("������������ ������ ��������� � ������ �������� �������: " + res)
}

function SummHigherSideDiag(mass) {
    var res = 0
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < n - i - 1; j++) {
            res += mass[i][j]
        }
    }
    WScript.StdOut.WriteLine("����� ��������� ���� �������� ���������: " + res)
}

function SortMatrix(sortmass) {
  var res = []
    for (var i = 1; i < n; i += 2) {
        sortmass[i][0] = 1
        if (i > 0) sortmass[i][i] = 4
    }
    for (var i = 0; i < n; i += 2) {
        for (var j = 0; j < n - 1; j++) {
            if ((i + j + 2) <= n) {
              sortmass[i][j] = 7
            }
            else {
              sortmass[i][j] = 4
            }
        }
        sortmass[i][n - 1] = 1
    }
}

function MulMatrix(matrixf, matrixs) {
    var mulmatr = []
    for (var i = 0; i < n; i++) {
        mulmatr[i] = []
    }
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < n; j++) {
            mulmatr[i][j] = 0
            for (var ii = 0; ii < n; ii++) {
                mulmatr[i][j] += (matrixf[i][ii] * matrixs[ii][j])
            }
        }
    }
    return mulmatr
}

function MatrixDet(mass, size) {
    var det = 0
    if (size == 1) det = mass[0][0]
    else
        for (var j = 0; j < size; j++) {
            var one
            if ((size + j) % 2 == 1) {
                one = -1
            } else {
                one = 1
            }
            var lolm = DoAlgDopSt(mass, 0, j, size - 1)
            det += (one * mass[0][j] * MatrixDet(lolm, size - 1))
        }
    return det
}

function DoAlgDopSt(mass, list, st, size) {
    var algdop = new Array(size);
    for (var i = 0; i < size; i++) {
        algdop[i] = new Array(size)
    }
    for (var i = 0; i < list; i++) {
        for (var j = 0; j < st; j++) {
            algdop[i][j] = mass[i][j]
        }
        for (var j = st; j < size; j++) {
            algdop[i][j] = mass[i][j + 1]
        }
    }
    for (var i = list; i < size; i++) {
        for (var j = 0; j < st; j++) {
            algdop[i][j] = mass[i + 1][j]
        }
        for (var j = st; j < size; j++) {
            algdop[i][j] = mass[i + 1][j + 1]
        }
    }
    return algdop
}

function ObrMatrix(mass, size) {
    var nmass = new Array(size)
    for (var i = 0; i < size; i++) {
        nmass[i] = new Array(size)
    }
    deta = MatrixDet(mass, size)
    if (size == 1) nmass[0][0] = 1 / deta
    else
        for (var i = 0; i < n; i++) {
            for (var j = 0; j < n; j++) {
                var tm = DoAlgDopSt(mass, j, i, size - 1)
                nmass[i][j] = MatrixDet(tm, size - 1)
                if ((i + j) % 2 == 1) nmass[i][j] *= (-1)
                nmass[i][j] /= deta
            }
        }
    return nmass
}