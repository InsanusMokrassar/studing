try{
	var mainArg = WScript.Arguments(0)
}
catch(e){
	mainArg = "0"
}
if (mainArg=="1")
{
	var shell = WScript.CreateObject("WScript.Shell")
	var enumerator = new Enumerator(shell.Environment)
	while(!enumerator.atEnd())
	{
		try{
			WScript.StdOut.WriteLine(enumerator.item())
		}
		catch(e){
			WScript.Echo("" + enumerator.item()); 
		}
		enumerator.moveNext()
	}
	end()
}
if (mainArg=="2")
{
	var PerSrSh = WScript.CreateObject("WScript.Shell")
	var folders = PerSrSh.SpecialFolders
	for (var i = 0; i < folders.Count(); i++) {
		try{
			WScript.StdOut.WriteLine(folders(i))
		}
		catch(e){
			WScript.Echo("" + folders(i)); 
		}
	}
	end()
}
WScript.StdOut.WriteLine("1 Выводит имена и содержимое всех системных переменных окружения")
WScript.StdOut.WriteLine("2 Выводит все специальные папки")
end()

function end(){
	try{
		WScript.StdOut.WriteLine("Для продолжения нажмите ввод...")
		WScript.StdIn.ReadLine()
	}
	catch(e){}
	WScript.Quit(0)
}