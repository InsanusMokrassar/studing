try{
	var mainArg = WScript.Arguments(0)
}
catch(e){
	mainArg = "0"
}
if (mainArg=="1")
{
	WScript.StdOut.WriteLine("Первое число: ")
	a = WScript.StdIn.ReadLine()
	WScript.StdOut.WriteLine("Второе число: ")
	b = WScript.StdIn.ReadLine()
	symb = " == "
	if (a > b){
		symb = " > "
	}
	if (a < b){
		symb = " < "
	}
	WScript.StdOut.WriteLine(a + symb + b)
	end()
}
if (mainArg=="2")
{
	WScript.StdOut.WriteLine("Путь: " + WScript.ScriptFullName)
	WScript.StdOut.WriteLine("Название: " + WScript.ScriptName)
	WScript.StdOut.WriteLine("Версия WSH: " + WScript.Version)
	end()
}
WScript.StdOut.WriteLine("1 Сравнение двух чисел, введенных с клавиатуры")
WScript.StdOut.WriteLine("2 Выводит на экран путь к исполняемому файлу сервера сценариев, имя запущенного сценария и версию WSH")
end()

function end(){
	WScript.StdOut.WriteLine("Для продолжения нажмите ввод...")
	WScript.StdIn.ReadLine()
	WScript.Quit(0)
}