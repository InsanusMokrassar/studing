@echo off
:l_read
cls
echo Choose an operation
echo 1. Output computer's name
echo 2. Output TCP/IP configuration
echo 3. Output DNS cash
echo 4. Clear DNS cash
set /p a=""
if %a% lss 1 (
goto l_read
)
if %a% gtr 4 (
goto l_read
)
if %a% equ 1 (
hostname
)
if %a% equ 2 (
ipconfig
)
if %a% equ 3 (
ipconfig /displaydns
)
if %a% equ 4 (
ipconfig /flushdns
)
pause