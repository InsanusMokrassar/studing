#!bin/sh
if test -e $1
then
	echo Файл найден
	if test -r $1 
		then
		echo Доступен для чтения
	fi
	if test -w $1 
		then
		echo Доступен для записи
	fi
	if test -e $1 
		then
		echo Доступен для исполнения
	fi
	exit 0
fi
echo Файл не найден