#!bin/sh
insert(){
	cd $srcFolder
	files=$(find -name "*")
	for f in $files
	do
		echo Проверка: $f
		if ! test -e $reserveFolder/$f
		then
		cp $f $reserveFolder
		echo Скопирован: $f
	fi
	done
}

update(){
	cd $srcFolder
	files="$(find -name "*")"
	for f in $files
	do
		echo Проверка: $f
		if test -e $reserveFolder/$f
		then
		cp $f $reserveFolder
		echo Заменён: $f
	fi
	done
}

delete(){
	cd $reserveFolder
	fileName=""
	files=$(find -name "*")
	for f in $(find -name "*")
	do
		
		echo Проверка: $f
		if ! test -e $srcFolder/$f
			then
			rm $reserveFolder/$f
		fi
	done
	cd $srcFolder
}

updateOldest(){
	cd $reserveFolder
	files=$(find -name "*")
	for f in $files
	do
		echo Проверка: $f
		if test $f -ot $srcFolder/$f
		then
		copy $f
		echo Заменён: $f
	fi
	done
	cd $srcFolder
}

menu(){
echo '1. Добавить новые файлы'
echo "2. Заменить существующие файлы"
echo "3. Удалить лишние файлы из резервной папки"
echo "4. Заменить старые файлы"
read check
if test $check -eq 1 
	then
	insert
fi
if test $check -eq 2 
	then
	update
fi
if test $check -eq 3 
	then
	delete
fi
if test $check -eq 4 
	then
	updateOldest
fi

}

openMenu(){
	repeat="y"
	trueVar="y"
	while true
	do
		menu
		echo "Продолжить работу?(y/n)"
		read repeat
		if test $repeat != $trueVar
			then
			break
		fi
	done
}

echo Заявленная резервная директория: $1

reserveFolder=$1
srcFolder=$(pwd)
if ! test -n $reserveFolder
	then
	echo Значение пусто
	reserveFolder=$(pwd)
fi
if ! test -e $reserveFolder
	then
	echo Папка не существует
	reserveFolder=$(pwd)
fi
	
echo Выбранная резервная директория: $reserveFolder

openMenu