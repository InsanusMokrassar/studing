from django.db import models
from django.contrib.auth.models import User

class Note(models.Model):
    title = models.TextField(max_length=128)
    text = models.TextField(max_length=255)
    owner = models.ForeignKey(User)

class Photos(models.Model):
    owner = models.ForeignKey(User, unique=True)
    file = models.ImageField(upload_to="images/")