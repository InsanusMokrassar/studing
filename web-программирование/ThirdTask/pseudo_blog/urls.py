from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from ThirdTask import settings
from . import views

urlpatterns = [
    url(r'^users', views.users, name='users'),
    url(r'^authentification', views.login_authentificate, name='auth'),
    url(r'^sign_in-up', views.sign_in_up, name='sign_in-up'),
    url(r'^logout', views.logout_request),
    url(r'^user$', views.user),
    url(r'^remove_note', views.remove_note),
    url(r'^create_note', views.create_note),
    url(r'^$', views.root),
    url(r'^redact_info', views.redact_info),
    url(r'^edit_profile', views.edit_profile),
    url(r'^try_to_registrate', views.registration_trying),
    url(r'^do_redact_note', views.redact_note),
    url(r'^redact_note', views.open_redact_note),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()