# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Hamster',
            fields=[
                ('name', models.TextField()),
                ('secondName', models.TextField()),
                ('id', models.IntegerField(serialize=False, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('title', models.TextField(max_length=128)),
                ('text', models.TextField(max_length=255)),
                ('id', models.IntegerField(serialize=False, primary_key=True)),
            ],
        ),
    ]
