# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pseudo_blog', '0011_photos'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photos',
            name='file',
            field=models.ImageField(upload_to='images/'),
        ),
    ]
