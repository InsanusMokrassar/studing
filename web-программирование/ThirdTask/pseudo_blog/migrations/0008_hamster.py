# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.auth.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('pseudo_blog', '0007_auto_20151202_1712'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hamster',
            fields=[
                ('user_ptr', models.OneToOneField(primary_key=True, parent_link=True, serialize=False, to=settings.AUTH_USER_MODEL, auto_created=True)),
                ('photoUrl', models.FileField(upload_to='')),
            ],
            options={
                'verbose_name': 'user',
                'abstract': False,
                'verbose_name_plural': 'users',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
