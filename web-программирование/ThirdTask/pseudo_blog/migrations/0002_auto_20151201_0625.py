# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pseudo_blog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hamster',
            name='id',
            field=models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True),
        ),
        migrations.AlterField(
            model_name='note',
            name='id',
            field=models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True),
        ),
    ]
