# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('admin', '0001_initial'),
        ('pseudo_blog', '0009_auto_20151209_1443'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hamster',
            name='user_ptr',
        ),
        migrations.DeleteModel(
            name='Hamster',
        ),
    ]
