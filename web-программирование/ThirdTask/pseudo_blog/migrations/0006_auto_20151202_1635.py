# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pseudo_blog', '0005_hamster_password'),
    ]

    operations = [
        migrations.AddField(
            model_name='hamster',
            name='login',
            field=models.TextField(unique=True, default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='hamster',
            name='name',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='hamster',
            name='secondName',
            field=models.TextField(default=''),
        ),
    ]
