# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pseudo_blog', '0004_auto_20151201_0647'),
    ]

    operations = [
        migrations.AddField(
            model_name='hamster',
            name='password',
            field=models.TextField(default=1234),
            preserve_default=False,
        ),
    ]
