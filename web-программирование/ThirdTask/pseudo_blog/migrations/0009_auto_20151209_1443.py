# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pseudo_blog', '0008_hamster'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hamster',
            name='photoUrl',
            field=models.URLField(),
        ),
    ]
