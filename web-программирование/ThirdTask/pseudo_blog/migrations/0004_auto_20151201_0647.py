# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pseudo_blog', '0003_note_owner_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='note',
            old_name='owner_id',
            new_name='owner',
        ),
    ]
