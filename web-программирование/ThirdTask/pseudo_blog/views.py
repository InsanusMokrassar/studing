import re
import uuid

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.db import IntegrityError, DataError
from django.http import HttpResponse
from django.shortcuts import render, redirect
# Create your views here.
from django.template import RequestContext, loader

from pseudo_blog.forms.NoteForm import NoteForm
from pseudo_blog.models import Note, Photos
from pseudo_blog.services import database_service
from pseudo_blog.services.database_service import SaveContent, RemoveContent


def users(request):
    notesBD = Note.objects.all()
    notes = []
    for note in notesBD:
        notes.append(NoteForm(note))
    notes = reversed(notes)
    template = loader.get_template('users.html')
    context = RequestContext(request, {
        'notes': notes,
    })
    return HttpResponse(template.render(context))

def sign_in_up(request):

    if request.user.is_authenticated():
        return redirect('/pseudo_blog/users')
    return render(request, 'sign_in-up.html')

def login_authentificate(request):
    try:
        user = authenticate(username=request.POST['login'], password=request.POST['password'])
        if user is None:
            raise
        login(request, user)
        return redirect('/pseudo_blog/users')
    except:
        template = loader.get_template('sign_in-up.html')
        context = RequestContext(request, {
                'response': 'Введена некорректная пара логин-пароль'
            })
        return HttpResponse(template.render(context))

def remove_note(request):
    try:
        if not request.user.is_authenticated():
            raise
        postId = request.GET['post_id']
        RemoveContent(request.user, postId)
    except:
        return redirect(request.META.get('HTTP_REFERER'))
    return redirect(request.META.get('HTTP_REFERER'))


def logout_request(request):
    logout(request)
    return redirect('/pseudo_blog/sign_in-up')

def registration_trying(request):
    template = loader.get_template('sign_in-up.html')
    context = RequestContext(request)
    response = {}
    for key in request.POST.keys():
        response[key] = request.POST[key]
    try:
        username = request.POST['login']
        password = request.POST['password']
        database_service.SaveHamster(username, password)
        user = authenticate(username=username, password=password)
        login(request, user)
        return redirect('/pseudo_blog/user')
    except IntegrityError:
        response['response'] = 'Логин или пароль некорректны. Данная ошибка может возникать, если логин занят.'
        context = RequestContext(request, response)
    except DataError:
        response['response'] = 'В одном из полей регистрации слишком много символов.'
        context = RequestContext(request, response)
    except:
        context = RequestContext(request, response)

    return HttpResponse(template.render(context))

def user(request):
    if not request.user.is_authenticated():
        redirect('/pseudo_blog/users')
    template = loader.get_template('user.html')
    try:
        response = {}
        id = ""
        try:
            id = request.GET['id']
        except:
            id = request.user.id
        id = int(id)
        user = User.objects.filter(id=id).first()
        notes = Note.objects.filter(owner=user)
        notes = reversed(notes)
        response['notes'] = notes
        response['id'] = id
        response['username'] = user.username
        response['email'] = user.email
        response['photo'] = Photos.objects.filter(owner=user).first()
        context = RequestContext(request, response)
    except:
        context = RequestContext(request)
    return HttpResponse(template.render(context))

def redact_note(request):
    response = request.POST
    id = response['id']
    try:
        targetNote = Note.objects.filter(id=id).first()
        if targetNote.owner.id == request.user.id:
            targetNote.text = response['content']
            targetNote.title = response['title']
            targetNote.save()
    except:
        return redirect('/pseudo_blog/redact_note?post_id='+id)
    return redirect('/pseudo_blog/user')

def open_redact_note(request):
    try:
        targetNote = NoteForm(Note.objects.filter(id=request.GET['post_id']).first())
        if (targetNote.owner.id != request.user.id):
            raise
        template = loader.get_template('redact_note.html')
        response = {}
        response['targetNote'] = targetNote
        context = RequestContext(request, response)
        return HttpResponse(template.render(context))
    except:
        return redirect('/pseudo_blog/users')

def edit_profile(request):
    login = request.POST['login']
    email = request.POST['email']
    avatar = Photos.objects.filter(owner=request.user).first()
    try:
        pattern = r'[\w]{1,}@[\w]{1,}\.[\w]{1,3}'
        email_regexp = re.compile(pattern)
        if (User.objects.filter(username=login) and not login == request.user.username) or (not email_regexp.findall(email) and not email == ""):
            raise
        request.user.username = login
        request.user.email = email
        try:
            avatar = Photos(file=request.FILES['avatar'], owner=request.user)
            old_avatar = Photos.objects.filter(owner=request.user).first()
            if old_avatar:
                old_avatar.delete()
            avatar.save()
        except:
            pass
        request.user.save()
        return redirect('/pseudo_blog/user')
    except:
        template = loader.get_template('redact_info.html')
        response = {}
        response['redact_login'] = login
        response['redact_email'] = email
        response['photo'] = avatar
        response['response'] = 'Вы ввели некорректные данные. Возможно, вы ввели некорректный e-mail либо логин занят.'
        context = RequestContext(request, response)
        return HttpResponse(template.render(context))

def redact_info(request):
    if not request.user.is_authenticated():
        return redirect('/pseudo_blog/users')
    template = loader.get_template('redact_info.html')
    response = {}
    response['redact_email'] = request.user.email
    response['redact_login'] = request.user.username
    response['photo'] = Photos.objects.filter(owner=request.user).first()
    context = RequestContext(request, response)
    return HttpResponse(template.render(context))

def create_note(request):
    if not request.user.is_authenticated():
        redirect('/pseudo_blog/users.html')
    try:
        title = request.POST['title']
        content = request.POST['content']
        SaveContent(title, content, request.user)
    except:
        print('error')
    return redirect('/pseudo_blog/user?id=' + str(request.user.id))

def root(request):
    return redirect("/pseudo_blog/users")