class NoteForm:
    id = -1
    text = ""
    title = ""
    owner = False
    photos = []

    def __init__(self, id = -1, text = "", title = "", photos = []):
        super().__init__()
        self.id = id
        self.text = text
        self.title = title
        self.photos = photos

    def __init__(self, note):
        self.id = note.id
        self.text = note.text
        self.title = note.title
        self.owner = note.owner
        # self.photos = note.photos