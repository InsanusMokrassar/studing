from django.contrib.auth.models import User

from pseudo_blog.models import Note


def SaveHamster(login, password, email=''):
    newHamster = User.objects.create_user(username=login, password=password, email=email)
    newHamster.save()
    return newHamster

def SaveContent(title, content, user):
    resContent = ""
    try:
        resContent = content[0:255]
        resTitle = title[0:26]
    except:
        resContent = content
        resTitle = title
    newNote = Note(text=resContent, title=resTitle, owner=user)
    newNote.save()

def RemoveContent(user, content):
    toRemove = Note.objects.filter(id=content, owner=user.id)
    toRemove.delete()

def SaveAdditionalInformation(inf = []):
    print('okay -.-')