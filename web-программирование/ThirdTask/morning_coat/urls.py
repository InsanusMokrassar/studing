from django.conf.urls import include, url
from . import views
from django.contrib import admin

urlpatterns = [
    url('^about_me/$', views.about_page),
    url('^university/$', views.university),
    url('^group/$', views.group)
]