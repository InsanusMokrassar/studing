from django.shortcuts import render

# Create your views here.
def about_page(request):
    return render(request, 'about_me.html')

def university(request):
    return render(request, 'university.html')

def group(request):
    return render(request, 'group.html')