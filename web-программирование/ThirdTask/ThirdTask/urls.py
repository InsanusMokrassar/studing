from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.root),
    url(r'^morning_coat/', include('morning_coat.urls')),
    url(r'^pseudo_blog/', include('pseudo_blog.urls')),
]
