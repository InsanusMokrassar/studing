package com.company;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Coder {

    private HashMap<String, String> codes;

    private List<String> start;
    private List<String> end;
    private List<String> waiting;
    private List<String> error;

    public static final String separator = "|";
    public static final String errorCode = "error";

    public Coder(){
        start = new ArrayList<>();
        end = new ArrayList<>();
        waiting = new ArrayList<>();
        error = new ArrayList<>();
        codes = new HashMap<>();
        codes.put("0000", "11110");
        codes.put("0001", "01001");
        codes.put("0010", "10100");
        codes.put("0011", "10101");
        codes.put("0100", "01010");
        codes.put("0101", "01011");
        codes.put("0110", "01110");
        codes.put("0111", "01111");
        codes.put("1000", "10010");
        codes.put("1001", "10011");
        codes.put("1010", "10110");
        codes.put("1011", "10111");
        codes.put("1100", "11010");
        codes.put("1101", "11011");
        codes.put("1110", "11100");
        codes.put("1111", "11101");
        start.add("11000");
        start.add("10001");
        end.add("01101");
        end.add("00111");
        waiting.add("11111");
        error.add("00100");
    }

    public List<String> decode4BTo5B(String code){
        List<String> result = new ArrayList<>();
        if (code == null || code.isEmpty()) {
            result.add(errorCode);
            return result;
        }
        List<Byte> symbols = toBitArray(code);
        String current = "";
        for (byte sym : symbols){
            current += sym + "";
            if (current.length() == 4){
                if (codes.containsKey(current)){
                    result.add(codes.get(current));
                }
                current = "";
            }
        }
        return result;
    }


    public List<Byte> decodeScrabling(String code){
        List<Byte> res = new ArrayList<>();
        List<Byte> inputBits = toBitArray(code);
        byte lastResXOR = 0;
        for (int i = 0; i < inputBits.size(); i++){
            lastResXOR = inputBits.get(i);
            if (res.size() > 2) {
                lastResXOR =xor(lastResXOR, res.get(i-3));
            }
            if (res.size() > 4) {
                lastResXOR =xor(lastResXOR, res.get(i-5));
            }
            res.add(lastResXOR);
        }
        return res;
    }

    public List<Byte> decodeB8ZS(String code){
        byte nonZero = 1;
        byte zeroCount = 0;
        List<Byte> res = new ArrayList<>();
        List<Byte> inputBits = toBitArray(code);
        for (Byte tmp : inputBits){
            if (tmp == 0){
                zeroCount++;
                if (zeroCount == 8){
                    for (byte i = 0; i < 3; i++){
                        res.add((byte)0);
                    }
                    res.add((byte)(nonZero*(-1)));
                    res.add(nonZero);
                    res.add((byte)0);
                    res.add(nonZero);
                    res.add((byte)(nonZero*(-1)));
                    zeroCount = 0;
                }
            }
            else {
                if (zeroCount > 0){
                    for (byte i = 0; i < zeroCount; i++){
                        res.add((byte)0);
                    }
                    zeroCount = 0;
                }
                res.add(nonZero);
                nonZero *= -1;
            }
        }
        return res;
    }

    public static Byte xor(byte a, byte b){
        if (a + b == 1)
            return 1;
        return 0;
    }

    public List<Byte> toBitArray(String in){
        char[] symbols = in.toCharArray();
        List<Byte> res = new ArrayList<>();
        for (char sym : symbols){
            byte tmp = parseSymbol(sym);
            if (tmp > -1)
                res.add(tmp);
        }
        return res;
    }

    public byte parseSymbol(char sym){
        byte res = -1;
        try {
            Byte symCode = Byte.parseByte(sym + "");
            if (symCode == 0 || symCode == 1){
                res = symCode;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

}
