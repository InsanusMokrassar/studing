package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Coder coder = new Coder();
        String code = "00001101";
        System.out.println("Please, write source code(length <= 65536)");
        Scanner scanner = new Scanner(System.in);
        try{
            String tmpCode = scanner.nextLine();
            System.out.println("You write:\n" + tmpCode);
            code = removeNotBit(tmpCode);
            System.out.println("Confirmed string:\n" + code);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("4B/5B:" + listOfStringToString(coder.decode4BTo5B(code)));
        System.out.println("Scrabling:" + listOfByteToString(coder.decodeScrabling(code)));
        System.out.println("B8ZS:" + listOfByteToString(coder.decodeB8ZS(code)));
    }

    public static String removeNotBit(String in){
        String res = "";
        char[] chars = in.toCharArray();
        for (char tmpChar : chars){
            try{
                byte code = Byte.parseByte(tmpChar+"");
                if (code == 0 || code == 1){
                    res += code;
                }
            }
            catch (Exception e){
            }
        }
        return res;
    }

    public static String listOfStringToString(List<String> objects){
        String res = "";
        for (String object : objects){
            res += object;
        }
        return res;
    }

    public static String listOfByteToString(List<Byte> objects){
        String res = "";
        for (Byte tmp : objects){
            res += tmp;
        }
        return res;
    }


}
