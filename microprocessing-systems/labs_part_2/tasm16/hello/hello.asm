name 	hello
title 	����� hello �� �����
stdin 	equ 0
stdout 	equ 1
stderr 	equ 2

cr 		equ 0dh
lf 		equ 0ah

_TEXT 	segment word public 'CODE'

		org 100h
		
		assume cs:_TEXT, ds:_TEXT, ss:_TEXT
		
print 	proc near
		
		mov ah, 40h
		mov bx, stdout
		mov cx, msg_len
		mov dx, offset msg
		int 21h
		
		mov ax, 4c00h
		int 21h
		
print 	endp

msg 	db cr, lf
		db 'Hello, world!', cr, lf
		
msg_len equ $-msg

_TEXT ends

end print