package com.insanus;

import com.insanus.interfaces.State;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<State> table = new ArrayList<>();
        HashMap<Integer, Integer> initList = new HashMap<>();
        initList.put(0, 1);//_ -> 1
        initList.put(1, 1);//word -> 1
        initList.put(2, 2);//numb -> 2
        initList.put(3, 3);//( -> 3
        initList.put(5, 5);//[ -> 5
        StandardState currentState = StandardState.init(initList, false);
        table.add(currentState);
        initList = new HashMap<>();
        initList.put(0, 1);//_ -> 1
        initList.put(1, 1);//word -> 1
        initList.put(2, 1);//numb -> 1
        initList.put(3, 3);//( -> 3
        initList.put(5, 5);//[ -> 5
        initList.put(7, 7);//, -> 1
        currentState = StandardState.init(initList, true);
        table.add(currentState);
        initList = new HashMap<>();
        initList.put(2, 2);//numb -> 2
        currentState = StandardState.init(initList, true);
        table.add(currentState);
        initList = new HashMap<>();
        initList.put(0, 3);//_ -> 3
        initList.put(1, 3);//word -> 3
        initList.put(2, 3);//numb -> 3
        initList.put(4, 4);//) -> 4
        initList.put(7, 3);//, -> 3
        currentState = StandardState.init(initList, false);
        table.add(currentState);
        initList = new HashMap<>();
        initList.put(0, 1);//_ -> 1
        initList.put(1, 1);//word -> 1
        initList.put(2, 1);//numb -> 1
        initList.put(3, 3);//( -> 3
        initList.put(5, 5);//[ -> 5
        currentState = StandardState.init(initList, true);
        table.add(currentState);
        initList = new HashMap<>();
        initList.put(2, 5);//numb -> 5
        initList.put(6, 6);//( -> 3
        currentState = StandardState.init(initList, false);
        table.add(currentState);
        initList = new HashMap<>();
        initList.put(0, 1);//_ -> 1
        initList.put(1, 1);//word -> 1
        initList.put(2, 1);//numb -> 1
        initList.put(3, 3);//( -> 3
        initList.put(5, 5);//[ -> 5
        currentState = StandardState.init(initList, true);
        table.add(currentState);
        initList = new HashMap<>();
        initList.put(1, 1);//word -> 1
        currentState = StandardState.init(initList, true);
        table.add(currentState);
        MinimalParser parser = MinimalParser.init(table);
        MinimalReader reader = MinimalReader.init();
        System.out.println(parser.match(reader));
    }
}
