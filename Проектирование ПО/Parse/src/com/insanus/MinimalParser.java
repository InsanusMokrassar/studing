package com.insanus;

import com.insanus.interfaces.Parser;
import com.insanus.interfaces.Reader;
import com.insanus.interfaces.State;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class MinimalParser implements Parser {
    List<State> table;
    Integer current;

    public static MinimalParser init(List<State> table){
        return new MinimalParser(table);
    }

    private MinimalParser(List<State> table){
        this.table = table;
        current = 1;
    }

    public boolean match(Reader r){
        while(true){
            try {
                Integer next = r.get();
                current = table.get(current).next(next);
            }
            catch (ReaderException e){
                try {
                    return table.get(current).isCompletive();
                }
                catch (NullPointerException e2){
                    return false;
                }
            }
            catch (StateException e){
                return false;
            }
            catch(NullPointerException e){
                return false;
            }
        }
    }
}
