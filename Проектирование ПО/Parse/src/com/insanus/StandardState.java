package com.insanus;

import com.insanus.interfaces.State;

import java.util.HashMap;

public class StandardState implements State {
    private HashMap<Integer, Integer> states;
    private Boolean completive;
    public static StandardState init(HashMap<Integer, Integer> input, Boolean completive){
        StandardState state = new StandardState(input, completive);
        return state;
    }

    private StandardState(HashMap<Integer, Integer> input, Boolean completive){
        this.states = input;
        this.completive = completive;
    }

    public Integer next(Integer inp) throws StateException{
        try{
            return states.get(inp);
        }
        catch (NullPointerException e){
            throw new StateException();
        }
    }

    public Boolean isCompletive() throws StateException{
        return completive;
    }

}
