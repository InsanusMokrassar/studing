package com.insanus.interfaces;

import com.insanus.StateException;

import java.io.IOException;

public interface State{
    public Integer next(Integer inp) throws StateException;
    public Boolean isCompletive();
}
