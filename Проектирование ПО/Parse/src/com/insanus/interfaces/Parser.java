package com.insanus.interfaces;

public interface Parser {
    public boolean match(Reader r);
}
