package com.insanus;

import com.insanus.interfaces.Reader;
import com.insanus.interfaces.State;

import java.util.ArrayList;
import java.util.List;

public class MinimalReader implements Reader {
    List<Integer> inputs;
    Integer current;
    public static MinimalReader init(){
        return new MinimalReader();
    }
    private MinimalReader(){
        inputs = new ArrayList<>();
        // inputs.add(1);//m
        // inputs.add(1);//a
        // inputs.add(1);//t
        // inputs.add(1);//r
        // inputs.add(1);//i
        // inputs.add(1);//x
        // inputs.add(7);//,
        // inputs.add(1);//m
        // inputs.add(1);//a
        // inputs.add(1);//t
        // inputs.add(1);//r
        // inputs.add(1);//i
        // inputs.add(1);//x
        // inputs.add(2);//2
        // inputs.add(3);//(
        // inputs.add(4);//)
        // inputs.add(0);//_
        current = 0;
    }

    public Integer get() throws ReaderException{
        try {
            Integer res = inputs.get(current);
            current++;
            return res;
        }
        catch (IndexOutOfBoundsException e){
            throw new ReaderException();
        }
    }

}
