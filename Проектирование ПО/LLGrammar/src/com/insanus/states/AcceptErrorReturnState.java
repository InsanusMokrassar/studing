package com.insanus.states;

import com.insanus.StateException;
import com.insanus.TermException;
import com.insanus.interfaces.LLReader;
import com.insanus.interfaces.State;
import com.insanus.interfaces.Term;

import java.util.EmptyStackException;
import java.util.Stack;

/**
 * Created by aleksey on 01.10.15.
 */
public class AcceptErrorReturnState implements State{
    private LLReader llReader;
    private Term term;
    private Stack<Integer> stack;

    public AcceptErrorReturnState(LLReader llReader, Term term, Stack<Integer> stack) {
        this.llReader = llReader;
        this.term = term;
        this.stack = stack;
    }

    public Integer next() throws StateException {
        Integer next = null;
        try {
            Integer currentSymb = llReader.get();
            term.have(currentSymb);
            accept();
            next = returnFunc();
        }
        catch (Exception e){
            error();
        }
        return next;
    }

    private void accept(){
        llReader.accept();
    }

    private Integer returnFunc(){
        return stack.pop();
    }

    private void error() throws StateException{
        throw new StateException();
    }
}
