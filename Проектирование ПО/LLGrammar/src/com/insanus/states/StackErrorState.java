package com.insanus.states;


import com.insanus.StateException;
import com.insanus.TermException;
import com.insanus.interfaces.LLReader;
import com.insanus.interfaces.State;
import com.insanus.interfaces.Term;

import java.util.Stack;

public class StackErrorState implements State{
    private LLReader llReader;
    private Integer next;
    private Term term;
    private Stack<Integer> stack;
    private Integer stackTarget;

    public StackErrorState(LLReader llReader, Integer next, Term term, Stack<Integer> stack, Integer current) {
        this.llReader = llReader;
        this.next = next;
        this.term = term;
        this.stack = stack;
        stackTarget = current + 1;
    }

    public Integer next() throws StateException{
        try {
            Integer currentSymb = llReader.get();
            term.have(currentSymb);
            stack();
        }
        catch (Exception e){
            error();
        }
        return next;
    }

    private void stack(){
        stack.add(stackTarget);
    }

    private void error() throws StateException{
        throw new StateException();
    }

}
