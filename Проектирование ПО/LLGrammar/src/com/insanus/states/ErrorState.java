package com.insanus.states;


import com.insanus.StateException;
import com.insanus.TermException;
import com.insanus.interfaces.LLReader;
import com.insanus.interfaces.State;
import com.insanus.interfaces.Term;

public class ErrorState implements State{
    private LLReader llReader;
    private Integer next;
    private Term term;

    public ErrorState(LLReader llReader, Integer next, Term term) {
        this.next = next;
        this.llReader = llReader;
        this.term = term;
    }

    public Integer next() throws StateException{
        try {
            Integer currentSymb = llReader.get();
            term.have(currentSymb);
        }
        catch (Exception e){
            error();
        }
        return next;
    }

    private void error() throws StateException{
        throw new StateException();
    }
}
