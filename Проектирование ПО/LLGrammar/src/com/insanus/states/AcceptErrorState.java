package com.insanus.states;

import com.insanus.StateException;
import com.insanus.TermException;
import com.insanus.interfaces.LLReader;
import com.insanus.interfaces.State;
import com.insanus.interfaces.Term;

public class AcceptErrorState implements State {
    private LLReader llReader;
    private Integer next;
    private Term term;

    public AcceptErrorState(LLReader llReader, Integer next, Term term) {
        this.llReader = llReader;
        this.next = next;
        this.term = term;
    }

    public Integer next() throws StateException{
        try {
            Integer currentSymb = llReader.get();
            term.have(currentSymb);
            accept();
        }
        catch (Exception e){
            error();
        }
        return next;
    }

    private void accept(){
        llReader.accept();
    }

    private void error() throws StateException{
        throw new StateException();
    }

}
