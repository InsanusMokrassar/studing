package com.insanus.states;

import com.insanus.StateException;
import com.insanus.TermException;
import com.insanus.interfaces.LLReader;
import com.insanus.interfaces.State;
import com.insanus.interfaces.Term;

import java.util.Stack;

public class ErrorReturnState implements State{
    private LLReader llReader;
    private Term term;
    private Stack<Integer> stack;

    public ErrorReturnState(LLReader llReader, Term term, Stack<Integer> stack) {
        this.llReader = llReader;
        this.term = term;
        this.stack = stack;
    }

    public Integer next() throws StateException{
        try {
            Integer currentSymb = llReader.get();
            term.have(currentSymb);
        }
        catch (Exception e){
            error();
        }
        return returnFunc();
    }

    private Integer returnFunc(){
        return stack.pop();
    }

    private void error() throws StateException{
        throw new StateException();
    }

}
