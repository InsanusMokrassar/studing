package com.insanus.states;

import com.insanus.StateException;
import com.insanus.TermException;
import com.insanus.interfaces.LLReader;
import com.insanus.interfaces.State;
import com.insanus.interfaces.Term;

public class ZeroState implements State{
    private LLReader llReader;
    private Integer next;
    private Integer errorNext;
    private Term term;

    public ZeroState(LLReader llReader, Integer next, Term term, Integer current) {
        this.llReader = llReader;
        this.next = next;
        this.term = term;
        errorNext = current + 1;
    }

    public Integer next() throws StateException{
        try {
            Integer currentSymb = llReader.get();
            term.have(currentSymb);
            return next;
        }
        catch (Exception e) {
            return errorNext;
        }
    }

}
