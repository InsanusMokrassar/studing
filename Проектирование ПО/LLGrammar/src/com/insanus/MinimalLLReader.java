package com.insanus;

import com.insanus.interfaces.LLReader;
import com.insanus.interfaces.Reader;

public class MinimalLLReader implements LLReader {
    private Integer symb;
    private Reader reader;
    private Boolean accept;

    public MinimalLLReader(Reader reader) {
        this.reader = reader;
        symb = reader.get();
        accept = false;
    }

    public Integer get() throws ReaderException{
        try {
            if (accept){
                try{
                    accept = false;
                    symb = reader.get();
                }
                catch (ReaderException e){
                    throw new ReaderException();
                }
            }
            return symb;
        }
        catch (IndexOutOfBoundsException e){
            throw new ReaderException();
        }
    }
    public void accept(){
        accept = true;
    }
}
