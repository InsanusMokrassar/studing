package com.insanus.interfaces;

import com.insanus.TermException;

public interface Term{
    void have(Integer symb) throws TermException;
}
