package com.insanus.interfaces;

import com.insanus.StateException;

public interface State{
    Integer next() throws StateException;
}
