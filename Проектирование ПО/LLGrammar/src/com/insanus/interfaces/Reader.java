package com.insanus.interfaces;

import com.insanus.ReaderException;

public interface Reader {
    public Integer get() throws ReaderException;
}
