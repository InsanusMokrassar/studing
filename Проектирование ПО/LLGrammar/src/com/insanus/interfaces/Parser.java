package com.insanus.interfaces;

public interface Parser {
    Boolean match() throws Exception;
}
