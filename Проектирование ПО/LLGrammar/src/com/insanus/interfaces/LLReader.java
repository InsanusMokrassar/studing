package com.insanus.interfaces;


import com.insanus.ReaderException;

public interface LLReader {
    Integer get() throws ReaderException;
    void accept() throws ReaderException;
}
