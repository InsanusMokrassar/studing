package com.insanus;

import com.insanus.interfaces.Reader;
import com.insanus.interfaces.State;

import java.util.ArrayList;
import java.util.List;

public class MinimalReader implements Reader {
    private List<Integer> inputs;
    private Integer current;


    public MinimalReader(){
        inputs = new ArrayList<>();
        inputs.add(0);//matrix (identifier)
        inputs.add(1);//=
        inputs.add(3);//2
        inputs.add(2);//i
        inputs.add(2);//x
        inputs.add(3);//2
        inputs.add(4);//;
        current = 0;
    }
    public Integer get() throws ReaderException{
        try {
            Integer symb = inputs.get(current);
            current++;
            return symb;
        }
        catch (IndexOutOfBoundsException e){
            throw new ReaderException();
        }
    }
}