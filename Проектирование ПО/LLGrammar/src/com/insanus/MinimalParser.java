package com.insanus;

import com.insanus.interfaces.Parser;
import com.insanus.interfaces.Reader;
import com.insanus.interfaces.State;

import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;


public class MinimalParser implements Parser {

    List<State> table;
    Integer currentState;
    Stack<Integer> stack;
    Reader reader;

    public MinimalParser(List<State> table, Stack<Integer> stack, Reader reader){
        this.table = table;
        this.stack = stack;
        this.reader = reader;
        currentState = 0;
    }

    public Boolean match() throws Exception{
        try {
            while (true) {
                Integer nextState = table.get(currentState).next();
                currentState = nextState;
                System.out.println("State: " + currentState + "; Stack: " + stack.toString());
            }
        }
        catch (StateException e){
            try {
                reader.get();
                return false;
            }
            catch (ReaderException e1){
                return true;
            }
        }
    }

}
