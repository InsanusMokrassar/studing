package com.insanus;


import com.insanus.interfaces.Term;

import java.util.List;

public class MinimalTerm implements Term{
    List<Integer> terms;
    public MinimalTerm(List<Integer> terms){
        this.terms = terms;
    }

    public void have(Integer symb) throws TermException{
        if (!terms.contains(symb)){
            throw new TermException();
        }
    }
}
