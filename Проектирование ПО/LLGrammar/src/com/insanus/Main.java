package com.insanus;

import com.insanus.interfaces.LLReader;
import com.insanus.interfaces.Reader;
import com.insanus.interfaces.State;
import com.insanus.states.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        Reader reader = new MinimalReader();
        LLReader llReader = new MinimalLLReader(reader);
        List<State> table = new ArrayList<>();
        //--0
        List<Integer> forTerms = new ArrayList<>();
        forTerms.add(0);
        table.add(new ErrorState(llReader, 3, new MinimalTerm(forTerms)));
        //--1
        forTerms = new ArrayList<>();
        forTerms.add(2);
        forTerms.add(3);
        table.add(new ZeroState(llReader, 7, new MinimalTerm(forTerms), 1));
        //--2
        forTerms = new ArrayList<>();
        forTerms.add(4);
        table.add(new ErrorState(llReader, 9, new MinimalTerm(forTerms)));
        //--3
        forTerms = new ArrayList<>();
        forTerms.add(0);
        table.add(new AcceptErrorState(llReader, 4, new MinimalTerm(forTerms)));
        //--4
        forTerms = new ArrayList<>();
        forTerms.add(1);
        table.add(new AcceptErrorState(llReader, 5, new MinimalTerm(forTerms)));
        //--5
        forTerms = new ArrayList<>();
        forTerms.add(2);
        forTerms.add(3);
        forTerms.add(4);
        table.add(new StackErrorState(llReader, 1, new MinimalTerm(forTerms), stack, 5));
        //--6
        forTerms = new ArrayList<>();
        forTerms.add(4);
        table.add(new AcceptErrorReturnState(llReader, new MinimalTerm(forTerms), stack));
        //--7
        forTerms = new ArrayList<>();
        forTerms.add(2);
        forTerms.add(3);
        table.add(new AcceptErrorState(llReader, 8, new MinimalTerm(forTerms)));
        //--8
        forTerms = new ArrayList<>();
        forTerms.add(2);
        forTerms.add(3);
        forTerms.add(4);
        table.add(new StackErrorState(llReader, 1, new MinimalTerm(forTerms), stack, 8));
        //--9
        forTerms = new ArrayList<>();
        forTerms.add(4);
        table.add(new ErrorReturnState(llReader, new MinimalTerm(forTerms), stack));
        MinimalParser parser = new MinimalParser(table, stack, reader);
        try {
            System.out.println(parser.match());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }



}
