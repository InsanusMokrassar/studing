Проблемы:
	1. Многопоточность
	2. БД
	3. Данные

CRUD - Create, Read, Update, Delete
принцип memento - хранить в бд данные одного типа.
пример - JSON:
	{
		'a':1,
		'b':[1, 2, 3]
	}
Тогда взаимодействие происходит так:
interface IObject{
	Object getValue(String name);
	void setValue(String name, Object value);
}

interface Field<T>{
	T this[IObject 0]{get;set;}
}

