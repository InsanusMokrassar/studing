clear;

function [k, right, sumOf]=first(n, m, targetMatrix)

	k = input('k=');
	right = -1
	while (right < 0) | (right > m)
		right = input('i=');
	end
	sumOf = 0;

	for i = 1:n
		for j = 1:m 
			if (targetMatrix(i, j) <= k) & (j <= right)
				sumOf = sumOf + targetMatrix(i, j)
			end
		end 
	end
endfunction

function [res]=second(targetMatrixX, targetMatrixT)
	res = targetMatrixT*(targetMatrixX')*(targetMatrixT^-1)
endfunction


mprintf('Функция должна возвращать сумму элементов матрицы,\n не больших k, расположенных не правее столбца с номером i.')

n = input('n = '); 
m = input('m = '); 


mprintf('Введите матрицу: '); 
for i = 1:n
	for j = 1:m 
		mprintf("[%i][%i] = ", i, j)
		targetMatrix(i,j)=input('');
	end 
end

[k, right, res]=first(n, m, targetMatrix)

mprintf("Сумма элементов матрицы, не больших %f = %f,\n расположенных не правее столбца с номером %f\n", k, res, right)

mprintf('Находит значение выражения T()X^T) T^-1 ,\n где X и T- матрицы m×m X^T – транспонированная матрица X.\n')

m = input('m = '); 

mprintf('Введите матрицу X: '); 
for i = 1:m
	for j = 1:m 
		mprintf("X[%i][%i] = ", i, j)
		targetMatrixX(i,j)=input('');
	end 
end

mprintf('Введите матрицу T: '); 
for i = 1:m
	for j = 1:m 
		mprintf("T[%i][%i] = ", i, j)
		targetMatrixT(i,j)=input('');
	end 
end

[res]= second(targetMatrixX, targetMatrixT)
mprintf('Значение выражения T()X^T) T^-1  = \n')
disp(res)
