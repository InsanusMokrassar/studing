x=[0 1 1.5 2 2.5 3 3.5 4 4.5 5];   //sm glava_11.pdf v samom konce
y=[12 10.1 11.58 17.4 30.68 53.6 87.78 136.9 202.5 287]; 
z=[x;y]; 
X=[0.720 0.777 0.700]; 
plot2d(x,y,-4);
koeff=splin(x,y);
Y=interp(X,x,y,koeff);
plot2d(X,Y,-3);
t=-0.1:0.1:5.1;
ptd=interp(t,x,y,koeff);
plot2d(t,ptd); 
