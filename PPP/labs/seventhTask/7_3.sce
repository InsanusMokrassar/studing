clear;
function [zr]=G(c,z)
	zr=z(2)-c(1)*z(1)^3-c(2)*z(1)-c(3)
endfunction

x=[0 0.4 0.8 1.2 1.6 2];
y=[1.2 2.2 3.0 6.0 7.7 13.6];
z=[x;y];
c=[0;0;0];

[f,s]=datafit(G,z,c);
r=regress(x,y);
k=sum((x-mean(x)).*(y-mean(y)))/sqrt(sum((x-mean(x))^2)*sum((y-mean(y))^2));
disp('A C D:');
disp(f);
disp('s=');
disp(s);
disp('Коэффициент регрессии=');
disp(r);
disp('Коэффициент корреляции=');
disp(k);

t=0.1:0.1:2.1; 
l=f(1)*t^3+f(2)*t+f(3)
plot2d(t,l);
plot2d(x,y,-8);
