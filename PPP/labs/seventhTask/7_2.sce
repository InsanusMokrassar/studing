deff('z=y1(x)','z=cos(x+0.5)-2');
deff('p=y(x)','p=asin(1+2*x)');
x=-5:0.01:1;
plot(x,y1(x),x,y(x));
clear;

function [y]=fun(x)
	y(1)=cos(x(1)+0.5)-x(2)-2; 
	y(2)=sin(x(2))-2*x(1)-1; 
endfunction

a=fsolve([-1 1],fun);
disp(a);
