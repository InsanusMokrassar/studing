clear;
function [zr]=G(c,z)
zr=z(2)-z(1)./(c(1).*z(1)-c(2))
endfunction

x=[3 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9];
y=[0.61 0.6 0.592 0.58 0.585 0.583 0.582 0.57 0.572 0.571];
z=[x;y];
c=[1;0];

[f,s]=datafit(G,z,c);
r=regress(x,y);
k=sum((x-mean(x)).*(y-mean(y)))/sqrt(sum((x-mean(x))^2)*sum((y-mean(y))^2));
disp('A B:');
disp(f);
disp('s=');
disp(s);
disp('koef regressii=');
disp(r);
disp('koef korrell=');
disp(k);

t=2.9:0.1:4; 
l=t./(f(1).*t-f(2))
plot2d(t,l);
plot2d(x,y,-8);
