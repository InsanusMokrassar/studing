A=[0.3 1 1.67 -2.3;3 5 7 -1;5 7 1 -3;7 1 3 -5];
B=[4; 0; 4; 16];
x=inv(A)*B;
disp('ans1=');
disp(x);
c=A*x;
disp('check=');
disp(c);
clear;

A=[5 -1 3;0 2 -1;-2 -1 0];
B=[3 7 -2;1 1 -2;0 1 3];
D=(A-B)^2*A+2*B;
disp('D=');
disp(D);
x=det(D);
if x==0
  disp('Matrica D neobratimaya'); 
else
  t=inv(D);
  disp('obratnaya=');
  disp(t);
end
clear;