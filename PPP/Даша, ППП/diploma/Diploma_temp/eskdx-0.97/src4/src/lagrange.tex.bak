\section{��������� ��������}
\subsection{����� ��������� �������� ��� ������� �� �������}
\point ���������� ������� $n$ ������� ��� �� �������. ����� � i-��
���� ��������� ��������� ���� $\mbf {F_i}$ ($i = 1, \ldots, n$).
���� �� ����� �������������, �� �������� ������� ������ �������
\eqref{newton2}, ����� ������� $m_i$, ����������� $\mbf{\dot v_i}$
� ������ $\mbf {F_i}$ ����� �� ����� ����������� $m_i \mbf{\dot
v_i} = \mbf {F_i}$. ��� ������� ������ ��������� $\mbf{\dot v_i} =
\frac{1}{m_i} \mbf {F_i}$ ����� ��������� �������������� ��
�������. ����� ����� ��������� �� ���� ������� � ����������
��������������� ������ $\mbf{R_i}$. ���� $\mbf{R_i}$ ����������
��������� ������ \cite{analmeh}. ����������� ������� ������, ���
���������, ������������ �� ���������

\begin{equation}
\label{f1} %
m_i \mathbf{\dot v}_i = \mathbf{F_i} + \mathbf{R_{ci}},
\end{equation}
��� ����������� �������.

� ������� �� ������� ������� �������� ���� $\mathbf{F_i}$
���������� ��������� ������. �������� ���� ������ �������� ���
��������� ������� �� �������, ��������� � ��������� ��� �������.

���������� ��� ������� ������� ����� ��������:
\begin{equation}
\label{f2} %
\mathcal{I}_i \mathbf{\dot w_i} = \mathbf{\tau_i} +
\mathbf{\tau_{ci}}
\end{equation}
\begin{ESKDexplanation}
\item[��� ] $\mathbf{\tau_i}$ --- ������ ������� ���, ����������� �
����;
\item $\mathbf{\tau_{ci}}$ --- ������ ��� �������.
\end{ESKDexplanation}


\point �������� ������ �������� ����������� ������� ������� �
��������� �������� ������� � ��� ������� ��� ��������� ��������
����� � ����������� �� ������� ��������� ���������� � ���������
��� �������.

��� ���������� ��� ������� ����� ������������ ������������,
���������� � \cite[\S 3]{analmeh}:

\begin{equation}
\label{constraintforce}%
\mathbf{R}_{i} = \sum_{k = 1}^s \lambda_{ik} \frac{\partial
\mbf{F_{ik}}}{\partial \mathbf{p}_i}
\end{equation}
\begin{ESKDexplanation}
\item[��� ] $\lambda_k$ --- ���������� ������� �������������� ����������
�������� $\lambda_i$,
\item $s$ --- ����� ������������ �������� ������� ������� ���.
\end{ESKDexplanation}

��������� \eqref{constraintforce} ����� �������� � ���������
�����, ��������� ��� �������� � �. \ref{vector_view6n} ���� �
������� ��� � ���� ������:

\begin{equation}
\label{constraint_forces}%
    \mathbf{R_i} = \mathbf{J}_i^T \mathbf{\lambda}_i
\end{equation}
\begin{ESKDexplanation}
\item[��� ] $\mathbf{J_i}$ --- �������, �������������� �������� i-�� ���� � �������,
�������������� �� ���� ������� $l \times 6$, $l$ --- �����
������������ ������ �������� �������,
\item $\mathbf{\lambda}_i$ ---
������ ���������� ��������.
\end{ESKDexplanation}

��� ���� ��� ������� ���������� ��������� ������������ ���:
\begin{equation}
\label{constraint_forces_total}%
    \mathbf{R} = \mathbf{J}^T \mathbf{\lambda}
\end{equation}

��������� ��������� \eqref{f1} � \eqref{f2} ��� ���� ��� �������
��������:
\begin{equation}
\label{newton_euler} \mathbf{M} \mathbf{\dot V} = \mathbf{R} +
\mathbf{F}_{ext}
\end{equation}
\begin{ESKDexplanation}
\item[��� ] $\mathbf{R}$ --- ������������ ������ ��� �������,
\item $\mathbf{F}_{ext}$ --- ������ ������� ���, ����������� �
�������.
\end{ESKDexplanation}

� ������������ ��������� \eqref{newton_euler} �
\eqref{constraint_forces} ���������� ��������� �������� �������
��� � ����������� �������:
\begin{equation}
\label{main_system}
\left\{%
\begin{aligned}
\mathbf{M} \mathbf{\dot V}& = \mathbf{J}^T \lambda +
\mathbf{F}_{ext} \\
\mathbf{J} \mathbf{V}& = \xi%
\end{aligned}
\right.
\end{equation}

\point ������� $\mbf J$, �������� � \eqref{main_system} �
\eqref{constraint_forces_total}, �������� �������� ��������� �����
$n$ ������ � $s$ �������:
\begin{equation}
\label{J}%
\mathbf J =  \left(
    \begin{matrix}
        \mathbf J_{11} & \mathbf J_{12} & \ldots & \mathbf
        J_{1n}\\
        \mathbf J_{21} & \mathbf J_{22} & \ldots & \mathbf
        J_{2n}\\
        \vdots & \vdots & \ddots & \vdots\\
        \mathbf J_{s1} & \mathbf J_{s2} & \ldots & \mathbf J_{sn}
    \end{matrix}
\right)
\end{equation}
\begin{ESKDexplanation}
\item[��� ] $\mathbf J_{ij}$ --- ������� ����� $i$, ��������������
�������� ��� ���� $j$.
\end{ESKDexplanation}

� ������ ������ ������� \eqref{J} ���������� �������� ������ ���
���������� $\mbf J_{ij}$ � $\mbf J_{ik}$ ($i = 1, \ldots, s$; $j,
k = 1, \ldots, n$), �.~�. � ������ ������ ��������������� ������
���������� (�. \ref{joint_def}).
