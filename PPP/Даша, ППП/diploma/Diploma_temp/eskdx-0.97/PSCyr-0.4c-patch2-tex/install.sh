#!/bin/sh
# The installation script of PSCyr package (for teTeX system).
# Before running the script, choose how you'll update the pscyr.map file
# (see below).

TEXMF=`kpsewhich -expand-var='$TEXMFMAIN'`
mkdir -p $TEXMF/{tex/latex,fonts/tfm/public,fonts/vf/public,fonts/type1/public,fonts/afm/public,doc/fonts}/pscyr
mv dvips/base/* $TEXMF/dvips/base
mv dvips/config/* $TEXMF/dvips/config
mv tex/latex/pscyr/* $TEXMF/tex/latex/pscyr
mv fonts/tfm/public/pscyr/* $TEXMF/fonts/tfm/public/pscyr
mv fonts/vf/public/pscyr/* $TEXMF/fonts/vf/public/pscyr
mv fonts/type1/public/pscyr/* $TEXMF/fonts/type1/public/pscyr
mv fonts/afm/public/pscyr/* $TEXMF/fonts/afm/public/pscyr
mv LICENSE doc/README.koi doc/PROBLEMS $TEXMF/doc/fonts/pscyr

VARTEXFONTS=`kpsewhich -expand-var='$VARTEXFONTS'`
rm -f $VARTEXFONTS/pk/modeless/public/pscyr/*

# Next, we need to update psfonts.map.  The simplest (but not the best)
# method is to uncomment the following two lines.  A better solution is
# to add the line "pscyr.map" to the extra_module section of updmap script
# and then run it.
#
#cd $TEXMF/dvips/config
#cat pscyr.map >> psfonts.map

mktexlsr

