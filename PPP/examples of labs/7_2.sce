deff('z=y1(x)','z=asin(0.2+1.2*x)-x');
deff('p=y(x)','p=-sqrt(1-x^2)');
x=-1:0.01:1;
plot(x,y1(x),x,y(x));
clear;

function [y]=fun(x)
y(1)=sin(x(1)+x(2))-1.2*x(1)-0.2; 
y(2)=x(1)^2+x(2)^2-1; 
endfunction

a=fsolve([0.65 0.75],fun);
b=fsolve([-1 -0.9],fun);
disp(a,b);